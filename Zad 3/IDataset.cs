﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Lv5zad3
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
