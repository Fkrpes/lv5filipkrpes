﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Lv5zad3
{
    class DataConsolePrinter
    {
        
        public void PrintToConsole(IDataset data) {
            IReadOnlyCollection<List<string>> dataToPrint = data.GetData();
            if (dataToPrint == null)
            {
                Console.WriteLine("Error because of insificient user privilege");
            }
            else
            {
                foreach (List<string> list in dataToPrint)
                {
                    foreach (string symbol in list)
                    {
                        Console.WriteLine(symbol + "\t");
                    }
                    Console.WriteLine("\n");
                }

            }
        }
    }
}
