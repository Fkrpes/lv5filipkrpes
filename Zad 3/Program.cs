﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace Lv5zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            

            DataConsolePrinter dataPrinter = new DataConsolePrinter();

            Dataset dataset = new Dataset("test.csv");
            Console.WriteLine("Print with base dataset class:");
            dataPrinter.PrintToConsole(dataset);

            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("test.csv");
            Console.WriteLine("Print with virtualproxydataset class:");
            dataPrinter.PrintToConsole(virtualProxyDataset);

            User userA = User.GenerateUser("UserA");
            ProtectionProxyDataset protectionProxyDatasetA = new ProtectionProxyDataset(userA);
            User userB = User.GenerateUser("userB");
            ProtectionProxyDataset protectionProxyDatasetB = new ProtectionProxyDataset(userB);

            Console.WriteLine("Print with protectionproxydataset ID 1");
            dataPrinter.PrintToConsole(protectionProxyDatasetA);

            Console.WriteLine("Print with protectionproxydataset ID 2");
            dataPrinter.PrintToConsole(protectionProxyDatasetB);


            Console.Read();
        
    }
    }
}
