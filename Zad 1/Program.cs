﻿using System;

namespace Lv5Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            Box buyingBasket = new Box("NewlyRelesed");
            Product manga = new Product("Jojo's Bizzare Adventure Part 8 - Jojolion",10,0.02);
            Product game = new Product("Metro Exodus",60,0.04);
            Product gamingmouse = new Product("Sharkoon Draconia",30,0.5);
            buyingBasket.Add(manga);
            buyingBasket.Add(game);
            buyingBasket.Add(gamingmouse);

            Console.WriteLine("Product Names:" + buyingBasket.Description()+"\n");
            Console.WriteLine("Total Product Weight:" + buyingBasket.Weight + "\n");
            Console.WriteLine("Product Price:" + buyingBasket.Price+"\n");
            
            ShippingService shipex = new ShippingService(6);

            Console.WriteLine("ShippingCost:" + shipex.GetShippingCost(buyingBasket)+"\n");
            Console.WriteLine("TotalPrice:"+(buyingBasket.Price+shipex.GetShippingCost(buyingBasket))+"\n");

            Console.ReadLine();

        }
    }
}
