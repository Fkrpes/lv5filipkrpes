﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv5Zad1
{
    class ShippingService
    {
        private double pricePerWeightUnit;
        public ShippingService(double pricePerWeightUnit) {
            this.pricePerWeightUnit = pricePerWeightUnit;
        }
        public double GetShippingCost(IShipable requestedItems) {
            return requestedItems.Weight * pricePerWeightUnit;
        }
    }
}
