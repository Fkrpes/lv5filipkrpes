﻿using System;

namespace Lv5Zad5
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme theme1 = new LightTheme();
            GreenTheme theme2 = new GreenTheme();
            ReminderNote note1 = new ReminderNote("BugBites", theme1);
            note1.Show();
            ReminderNote note2 = new ReminderNote("Go!Go!Zeppelin!", theme2);
            note2.Show();
            Console.Read();
        }
    }
}
