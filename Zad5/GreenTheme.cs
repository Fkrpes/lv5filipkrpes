﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv5Zad5
{
    class GreenTheme:ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Black;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
        }
        public string GetHeader(int width)
        {
            return new string('~', width);
        }
        public string GetFooter(int width)
        {
            return new string('~', width);
        
    }
}
}
